﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : MonoBehaviour {

	// Use this for initialization
	private Rigidbody2D rb2d = null ;
	private Animator anim;
	public GameObject player;
    private float move = 1f;
	private bool flipped = false;
	public float maxS = 100f;
	public float alcance = 20.0f;
	private float positionX = 0f;
	void Start () {
		rb2d = GetComponent < Rigidbody2D >();
        anim = GetComponent < Animator >();
		positionX = rb2d.position.x;
	}
	
	public void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.name == "player"){
			if ((player.GetComponent<MyController>().shield == true) && (player.GetComponent<MyController>().attacking != 0f)){
				anim.SetBool("dead_zombiii", true);
				move = 0f;
				Destroy(this,1f);
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if ((rb2d.position.x >= positionX+alcance && !flipped) || (rb2d.position.x <= positionX-alcance && flipped)){
			flipped = !flipped;
            this.transform.rotation = Quaternion.Euler(0,flipped?180:0, 0);
			move = -move;
		}
		rb2d.velocity = new Vector2(move*3*maxS*Time.fixedDeltaTime, 0);
	}
}
