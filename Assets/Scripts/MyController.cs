﻿using System . Collections ;
using System . Collections . Generic ;
using UnityEngine ;
[ RequireComponent ( typeof ( Rigidbody2D ))]
public class MyController : MonoBehaviour {
    private Rigidbody2D rb2d = null ;
    private Animator anim;
    public GameObject katana;
    private float move = 0f;
    public float attacking = 0f;
    private float jumping = 0f;
    private bool flipped = false;
    public bool grounded = true;
    private bool muerto = false;
    public float maxS = 100f;
    public bool shield = false; 
    // Use this for initialization
    void Awake () {
        rb2d = GetComponent < Rigidbody2D >();
        anim = GetComponent < Animator >();
    }
    public void OnCollisionEnter2D(Collision2D coll) {
        if (coll.gameObject.tag == "muerte"){
            if (shield && attacking!=0){
                muerto = false;
            }else{
                muerto = true;
                anim.SetBool("dead", true);
                Destroy(this,1f);
            }
        }else{
            muerto = false;
        }

        if (coll.gameObject.tag == "katana"){
            shield = true;
            Destroy(katana,0f);
        }else{
           // shield = false;
        }
    }

    // Update is called once per frame
    void FixedUpdate () {
        /*/if((rb2d.position.x < 4281.0f)&&(rb2d.position.y < -213.99)){
            muerto = true;
        }*/
        if (shield == false){
            //primero checkeo de inputs
            move = Input .GetAxis("Horizontal");
            attacking = Input.GetAxis("Attack");
            jumping = Input.GetAxis("Vertical");
            
            if (rb2d.velocity.y > 0.001f || rb2d.velocity.y < -0.001f){//si la velocidad en y es diferente de 0 significa que esta saltando asi que no puede volver a saltar
                grounded = false;
            }else{
                grounded = true;
            }
            if (attacking != 0f){//si ataca se bloquea el movimiento y se cambia la animacion a atacar
                move = 0f;
            }else{
                //anim.SetBool("attack", false);
            }
            if (jumping != 0f && grounded){
                if (muerto){
                    return;
                }else{
                    move = 0f;
                    rb2d.velocity = new Vector2(jumping*2*maxS*Time.fixedDeltaTime, 60);
                }
            }else{
                //anim.SetBool("jump", false);
            }
            if (muerto){
                rb2d.velocity = new Vector2(0,0);
            }else{
                rb2d.velocity = new Vector2(move*2*maxS*Time.fixedDeltaTime, rb2d.velocity.y);
            }
            if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < - 0.001f ) {
                if ((rb2d.velocity.x < - 0.001f && !flipped) || (rb2d.velocity.x > - 0.001f && flipped)) {
                    flipped = !flipped;
                    this.transform.rotation = Quaternion.Euler(0,flipped?180:0, 0);
                }
                }else{
                    //anim.SetBool("run", false);
            }
        }
        else{// shield
            //primero checkeo de inputs
            move = Input .GetAxis("Horizontal");
            attacking = Input.GetAxis("Attack");
            jumping = Input.GetAxis("Vertical");
            
            if (rb2d.velocity.y > 0.001f || rb2d.velocity.y < -0.001f){//si la velocidad en y es diferente de 0 significa que esta saltando asi que no puede volver a saltar
                grounded = false;
            }else{
                grounded = true;
            }
            if (attacking != 0f){//si ataca se bloquea el movimiento y se cambia la animacion a atacar
                move = 0f;
            }else{
                //anim.SetBool("attack", false);
            }
            if (jumping != 0f && grounded){
                if (muerto){
                    return;
                }else{
                    move = 0f;
                    rb2d.velocity = new Vector2(jumping*2*maxS*Time.fixedDeltaTime, 30);
                }
            }else{
                //anim.SetBool("jump", false);
            }
            if (muerto){
                rb2d.velocity = new Vector2(0,0);
            }else{
                rb2d.velocity = new Vector2(move*2*maxS*Time.fixedDeltaTime, rb2d.velocity.y);
            }
            if (rb2d.velocity.x > 0.001f || rb2d.velocity.x < - 0.001f ) {
                if ((rb2d.velocity.x < - 0.001f && !flipped) || (rb2d.velocity.x > - 0.001f && flipped)) {
                    if (muerto){
                        return;
                    }else{
                        flipped = !flipped;
                        this.transform.rotation = Quaternion.Euler(0,flipped?180:0, 0);
                    }
                }
            }else{
                //anim.SetBool("run", false);
            }
        }

        //ANIMATION CONTROLLER
        if(rb2d.velocity.x > 0.001f || rb2d.velocity.x < -0.001f){//movimiento en x
            if(grounded){//moverse en el suelo
                if (attacking != 0f){//si ataca se cambia la animacion a atacar
                anim.SetBool("attack", true);
                anim.SetBool("run", false);
                anim.SetBool("jump", false);
                anim.SetBool("idle", false);
                }else{//animacion de correr
                    anim.SetBool("run", true);
                    anim.SetBool("attack", false);
                    anim.SetBool("jump", false);
                    anim.SetBool("idle", false);
                }
            }else{//moverse en el aire
                if (attacking != 0f){//si ataca se cambia la animacion a atacar
                anim.SetBool("attack", true);
                anim.SetBool("run", false);
                anim.SetBool("jump", false);
                anim.SetBool("idle", false);
                }else{//animacion de correr
                    anim.SetBool("run", false);
                    anim.SetBool("attack", false);
                    anim.SetBool("jump", true);
                    anim.SetBool("idle", false);
                }
            }
        }else{//no hay velocidad x
            if(attacking != 0f){
                anim.SetBool("run", false);
                anim.SetBool("attack", true);
                anim.SetBool("jump", false);
                anim.SetBool("idle", false);
            }else{
                if(grounded){//quieto
                    anim.SetBool("run", false);
                    anim.SetBool("attack", false);
                    anim.SetBool("jump", false);
                    anim.SetBool("idle", true);
                }else{//saltando
                    anim.SetBool("run", false);
                    anim.SetBool("attack", false);
                    anim.SetBool("jump", true);
                    anim.SetBool("idle", false);
                }
            }
        }
    }
}